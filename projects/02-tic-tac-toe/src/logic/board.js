import { WINNER_COMBOS } from '../constants'

export const checkWinner = (boardToCheck) => {
  // Revisamos todas las combinaciones ganadoras
  // Para ver si ganó alguno de los 2 (X u O)
  for (const combo of WINNER_COMBOS) {
    const [a, b, c] = combo

    if (
      boardToCheck[a] &&
      boardToCheck[a] === boardToCheck[b] &&
      boardToCheck[c]
    ) {
      return boardToCheck[a]
    }
  }

  // Si no hay ganador
  return null
}

export const checkEndGame = (newBoard) => {
  // Revisamos si hay un empate
  // si no hay más espacios vacíos
  // en el tablero

  return newBoard.every((square) => square !== null)
}
