import { useEffect, useState } from "react";

const CAT_PREFIX_URL = "https://cataas.com";

export function useCatImage({ fact }) {
  const [imageUrl, setImageUrl] = useState();

  // Para recuperar la imagen cada vez que tenemos una cita nueva
  useEffect(() => {
    if (!fact) return;

    const firstWord = fact.split(" ", 1).join("");
    fetch(
      `https://cataas.com/cat/says/${firstWord}?size=50&color=red&json=true`
    )
      .then((res) => res.json())
      .then((data) => {
        const { url } = data;
        setImageUrl(`${CAT_PREFIX_URL}${url}`);
      });
  }, [fact]);

  return { imageUrl };
}
