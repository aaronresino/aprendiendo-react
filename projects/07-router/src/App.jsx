import { lazy, Suspense } from 'react'

import Route from './Route'
import Router from './Router'

const LazyAboutPage = lazy(() => import('./pages/About.jsx'))
const LazyHomePage = lazy(() => import('./pages/Home.jsx'))
const LazySearchPage = lazy(() => import('./pages/Search.jsx'))
const Lazy404Page = lazy(() => import('./pages/404.jsx'))

const appRoutes = [
  {
    path: '/search/:query',
    Component: LazySearchPage
  }
]

function App () {
  return (
    <main>
      <Suspense fallback={null}>
        <Router routes={appRoutes} defaultComponent={Lazy404Page}>
          <Route path='/' Component={LazyHomePage} />
          <Route path='/about' Component={LazyAboutPage} />
        </Router>
      </Suspense>
    </main>
  )
}

export default App
