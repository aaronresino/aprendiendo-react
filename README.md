<div align="center">

<img alt="Curso de React js desde cero y con proyectos" src="https://res.cloudinary.com/practicaldev/image/fetch/s--wCGgterD--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://www.freecodecamp.org/news/content/images/size/w2000/2020/02/Ekran-Resmi-2019-11-18-18.08.13.png" width="500" />

# Aprendiendo React ⚛️
</div>

## Proyectos

- 01: [Twitter Follow Card](projects/01-twitter-follow-card/)
- 02: [Tic Tac Toe](projects/02-tic-tac-toe/)
- 03: [Mouse Follower](projects/03-mouse-follower)
- 04: [Prueba técnica](projects/04-react-prueba-tecnica)
- 05: [Prueba técnica de buscador utilizando una API](projects/05-react-buscador-peliculas)
- 06: [Creación de un ecommerce con carrito de compras](projects/06-shopping-cart)
- 07: [Creación de un React Router desde cero](projects/07-router)
