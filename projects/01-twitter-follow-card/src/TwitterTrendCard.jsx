function numberWithSeparator(number, character = '.') {
  console.log('numberWithSeparator')
  return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, character);
}

const formatTweetsNumber = (tweetsNumber) => {
  console.log('formatTweetsNumber')
  if (tweetsNumber > 9999) {
    return `${numberWithSeparator(tweetsNumber, ',').substring(0, 4)} mil Tweets`

  } else {
    return `${numberWithSeparator(tweetsNumber)} Tweets`
  }
}

export function TwitterTrendCard({ category, content, tweetsNumber }) {
  console.log('TwitterTrendCard')
  return (
    <article className="tw-trendCard">
      <header>{ category }</header>
      <div className="tw-trendCard-content">{ content }</div>
      {tweetsNumber && <footer>{ formatTweetsNumber(tweetsNumber) }</footer>}
    </article>
  )
}
