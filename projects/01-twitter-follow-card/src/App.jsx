import "./App.css";

import { TwitterFollowCard } from "./TwitterFollowCard";
import { TwitterTrendCard } from "./TwitterTrendCard";

const users = [
  {
    userName: "midudev",
    name: "Miguel Ángel Durán",
    isFollowing: true,
  },
  {
    userName: "pheralb",
    name: "Pablo H.",
    isFollowing: false,
  },
  {
    userName: "PacoHdezs",
    name: "Paco Hdez",
    isFollowing: false,
  },
  {
    userName: "TMChein",
    name: "Tomás",
    isFollowing: true,
  },
];

const trends = [
  {
    id: 1,
    category: "Deportes · Tendencia",
    content: "Bellerín"
  },
  {
    id: 2,
    category: "Entretenimiento · Tendencia",
    content: "Jim Carrey",
    tweetsNumber: 1939,
  },
  {
    id: 3,
    category: "Tendencias",
    content: "Babo",
    tweetsNumber: 24400,
  },
  {
    id: 4,
    category: "Tendencia en España",
    content: "#JorgeJaAlCongelador",
    tweetsNumber: 4445,
  },
  {
    id: 5,
    category: "Tendencia en España",
    content: "ME VOY DE TWITTER",
    tweetsNumber: 15500,
  },
];

export function App() {
  const formatUserName = (userName) => `@${userName}`;

  return (
    <article className="App">
      <section className="follow-list">
        <h3>Listado de seguidores</h3>
        {users.map(({ userName, name, isFollowing }) => {
          return (
            <TwitterFollowCard
              key={userName}
              formatUserName={formatUserName}
              userName={userName}
              initialIsFollowing={isFollowing}
            >
              {name}
            </TwitterFollowCard>
          );
        })}
      </section>
      <section className="trend-list">
        <h3>Listado de tendencias</h3>
        {trends.map((trend) => (
          <TwitterTrendCard
            key={trend.id}
            category={trend.category}
            content={trend.content}
            tweetsNumber={trend.tweetsNumber}
          />
        ))}
      </section>
    </article>
  );
}
