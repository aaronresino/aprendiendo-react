# Prueba técnica para Juniors y Trainees de React en Live Coding.

APIs:

- Facts Random: https://catfact.ninja/fact
- Imagen random: https://cataas.com/cat/says/hello

- Recupera un hecho aleatorio de gatos de la primera API
- Recuperar la primera palabra del hecho
- Muestra una imagen de un gato con la primera palabra

<!-- 1.- Creamos el proyecto con Vite, pero creamos uno de Vanilla. Ejecutamos el siguiente comando: `npm create vite@latest` a la hora de seleccionar el framework, seleccionaremos `Vanilla` y a continuación nos pedirá que seleccionemos con qué lo vamos a desarrollar, JavaScript o TypeScript. Elegimos Javascript.

2.- Una vez creado, instalamos el plugin de react de Vite. Con el siguiente comando: `npm i @vitejs/plugin-react -E`

3.- Una vez tengamos todo lo anterior instalado, para iniciar el proyecto de React necesitaremos 2 dependencias, `React` y `React-DOM`. Con el siguiente comando instalaremos las 2 dependencias: `npm i react react-dom -E`

4.- Ahora crearemos la configuración de Vite. Creamos el fichero `vite.config.js` en la raíz de nuestro proyecto. El contenido del fichero será el siguiente:

import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react'

export default defineConfig({
  plugins: [react()]
})

5.- Una vez tengamos todo lo anterior. En el archivo `main.js` tenemos que modificarlo para que pueda renderizar nuestra aplicación en el archivo `index.html`. Tenemos que cambiar la extensión a `main.jsx` ya que no es capaz de transpilar el código. Tendremos también que cambiar en el `index.html` la importación del archivo.

6.- Una vez consigamos cargar algo en nuestra pantalla. Lo siguiente a instalar es el linter. Utilizaremos standard. Lo instalamos con el siguiente comando: `npm i standard -D`. Para poder usarlo en nuestro proyecto, nos vamos al `package.json` e insertamos el siguiente trozo de código:

"eslintConfig": {
	"extends": "./node_modules/standard/eslintrc.json"
}

7.- Por último nos creamos nuestro punto de entrada a la app. Creamos un directorio `src` en el raíz del proyecto y creamos nuestro primer componente que será el punto de entrada `App.jsx`. -->
