import Link from '../Link'

export default function AboutPage () {
  return (
    <>
      <h1>About</h1>
      <div>
        <p>¡Hola! Me llamo Aarón RJ y estoy creando un clon de React Router.</p>
        <img src='https://pbs.twimg.com/profile_images/1309012089730629633/BK05j6Tj_400x400.jpg' alt='Foto de aaronresino' />
      </div>
      <Link to='/'>Ir a la home</Link>
    </>
  )
}
